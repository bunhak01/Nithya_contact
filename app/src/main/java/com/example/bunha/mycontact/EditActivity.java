package com.example.bunha.mycontact;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


public class EditActivity extends AppCompatActivity {
    EditText etName;
    EditText etNumber1;
    EditText etNumber2;
    ImageView img1, img2;
    Button btnSubmit, btnDelete,btnfav;
    Person p;

    PersonDbHelper personDbHelper;
    FavDbHelper favDbHelper;
    int i, pos,active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pos = extras.getInt("pos");
            active=extras.getInt("active");
        }
        i = 0;
        etName = (EditText) findViewById(R.id.etName);
        etNumber1 = (EditText) findViewById(R.id.etNumber1);
        etNumber2 = (EditText) findViewById(R.id.etNumber2);
        btnSubmit = (Button) findViewById(R.id.btnSave);
        btnfav= (Button) findViewById(R.id.btnFav);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        img1= (ImageView) findViewById(R.id.imgcall1);
        img2= (ImageView) findViewById(R.id.imgcall2);

        personDbHelper = new PersonDbHelper(this);
        favDbHelper=new FavDbHelper(this);
        if(active==0){
            p = personDbHelper.findPersonById(pos);
        }
        else if(active==1){
            p = favDbHelper.findPersonById(pos);
        }
        btnSubmit.setText("Edit");
        etName.setEnabled(false);
        etNumber1.setEnabled(false);
        etNumber1.setEnabled(false);
        btnDelete.setVisibility(View.INVISIBLE);
        if(active==1){
            btnfav.setVisibility(View.INVISIBLE);
            btnSubmit.setVisibility(View.INVISIBLE);
        }

        etName.setText(p.getName());
        etNumber1.setText(p.getNumber1());
        etNumber2.setText(p.getNumber2());

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNumber1.getText().toString().trim()==""){
                    return;
                }
                String uri = "tel:" + etNumber1.getText().toString().trim();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));
                if (ActivityCompat.checkSelfPermission(EditActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                EditActivity.this.startActivity(intent);
            }
        });

        btnfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(EditActivity.this, "Add to Favourite Successfully", Toast.LENGTH_SHORT).show();
                Person pp = new Person(p.id, etName.getText().toString(), etNumber1.getText().toString(),etNumber2.getText().toString());
                if (favDbHelper.addPerson(pp)){

                    Toast.makeText(EditActivity.this, "Add to Favourite Successfully", Toast.LENGTH_SHORT).show();
                    finish();


                } else{
                    Toast.makeText(EditActivity.this, "Failed....!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNumber2.getText().toString().trim()==""){
                    return;
                }
                String uri = "tel:" + etNumber2.getText().toString().trim();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));
                if (ActivityCompat.checkSelfPermission(EditActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                EditActivity.this.startActivity(intent);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i==0){
                    btnSubmit.setText("Save");
                    etName.setEnabled(true);
                    etNumber1.setEnabled(true);
                    etNumber1.setEnabled(true);
                    btnDelete.setVisibility(View.VISIBLE);
                    i=1;
                    return;
                }
                Person p = new Person(pos,etName.getText().toString(), etNumber1.getText().toString(),etNumber2.getText().toString());
                if (personDbHelper.updatePerson(p)){
                    favDbHelper.updatePerson(p);
                    Toast.makeText(EditActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();
                    i=0;
                    btnSubmit.setText("Edit");
                    etName.setEnabled(false);
                    etNumber1.setEnabled(false);
                    etNumber1.setEnabled(false);
                    btnDelete.setVisibility(View.INVISIBLE);
                } else{
                    Toast.makeText(EditActivity.this, "Failed....!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personDbHelper.deletePerson(p.id);
                favDbHelper.deletePerson(p.id);
                finish();
            }
        });
    }
}
