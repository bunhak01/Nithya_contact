package com.example.bunha.mycontact;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fav extends AppCompatActivity {

    EditText etName;
    EditText etNumber1;
    EditText etNumber2;
    Button btnfavourite;

    FavDbHelper favDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_favorite);

        etName = (EditText) findViewById(R.id.etName);
        etNumber1= (EditText) findViewById(R.id.etNumber1);
        etNumber2= (EditText) findViewById(R.id.etNumber2);
        btnfavourite= (Button) findViewById(R.id.btnFav);
        favDbHelper = new FavDbHelper(this);

        btnfavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Fav.this, "Add to Favourite Successfully", Toast.LENGTH_SHORT).show();
                Person p = new Person(etName.getText().toString(), etNumber1.getText().toString(),etNumber2.getText().toString());
                if (favDbHelper.addPerson(p)){
                    Toast.makeText(Fav.this, "Add to Favourite Successfully", Toast.LENGTH_SHORT).show();
                    finish();

                } else{
                    Toast.makeText(Fav.this, "Failed....!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
